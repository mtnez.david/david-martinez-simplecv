% A simple class and I hope clear as well to write
% a 2 column cv. It includes a block list environment
% which allows to add experience block with title and
% date, as well as a custom entry command to be used
% inside a sidelist environment based on description.  
%
% This document is inspired from the Nicola Fontana's
% two columns cv 
% https://github.com/ntd/tccv
% and the latex cv class AltaCV
% https://github.com/liantze/AltaCV
%
%
% Author: David Martinez Martinez

\ProvidesClass{simplecv}


\PassOptionsToClass{\CurrentOption}{scrartcl}
\LoadClass[a4paper,10pt,parskip]{scrartcl}

% Basic packages
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{etoolbox}
\RequirePackage[english]{babel}

% Additional packages. xcolor for sections and links, enumitem for custom descriptions,
% marvosym for phone and email icons, cuted for one column title printout and hyperrref.
\RequirePackage{xcolor,enumitem,marvosym,cuted,hyperref,fontawesome,graphicx}
\RequirePackage[square,sort,comma,numbers]{natbib}


% Define colors for sections and links
\definecolor{sections}{rgb}{0,0.2,.5}
\definecolor{links}{rgb}{0,.4,.9}


% Additional page configuration
\RequirePackage[hmargin=1.25cm,top=0.4cm, bottom=1cm,twocolumn,columnsep=0.8cm]{geometry}
\pagestyle{empty}
\setlength{\parskip}{0em}
\renewcommand{\familydefault}{\sfdefault}
\setcounter{secnumdepth}{-1}


% Redefine section command with the color given by sections adding a rule 
\let\oldsection\section
\def\section#1{\oldsection{\texorpdfstring{\color{sections}\uppercase{\normalfont\large{#1}}}{}}\vspace{-0.8em}{\color{sections}\hrule} \medskip}


% Define list environment for experience section. Example:
% \begin{blocklist}
% \block{year}{company}{position}
% Additional information
% \end{blocklist}
\newenvironment{blocklist}{%
\newcommand\block[3]{%
\bigskip
 \hfill{\rmfamily\textsc{##1}}\\[2pt]
{{\bfseries {##3} at \emph{##2}} \\[2pt]} } } {}

% Include a contact line with name and phone number
% \contact{David}{phone}{email}
% Note phone is optional but argument needs to be present
\newcommand\contact[3]{
    \ifx&#2&%
       Contact: \hfill \Letter~\href{mailto:#3}{#3} \\[5pt]
    \else
       Contact: \Letter~\href{mailto:#3}{#3} \hfill \Telefon~ #2 \\[5pt]
    \fi
}


% Define a sidelist environment where an item key appears at the left of each 
% entry. Also add a bold title line to the item command as optional argument
% Example
% \begin{sidelist}
% \entry[title]{year}
% Additional information
% \end{sidelist}
\newcommand{\entry}[2][]{\item[{\rmfamily\textsc{#2}}\hspace{1ex} \textbf{#1}]}
\newenvironment{sidelist}[1][6em]{%
\begin{description}[font=\normalfont,leftmargin=#1,style=nextline]}
{\end{description}}



% Define address, phone and email field to make the cv title
\def\address#1{\def\theaddress{#1}}
\def\phone#1{\def\thephone{#1}}
\def\email#1{\def\theemail{\href{mailto:#1}{#1}}}
\def\birth#1{\def\thebirth{#1}}
\def\country#1{\def\thecountry{#1}}
\def\picture#1{\def\thepicture{#1}}
% Custom maketitle command which prints the title, subtitle and contact information
\makeatletter
\def\@maketitle{%
  \newpage
  \null
   \begin{strip}
      \begin{minipage}{0.45\linewidth}
    \huge \@title\par
    \smallskip
    \large \@subtitle \par
    \bigskip
  \end{minipage}\hfill \begin{minipage}{0.45\linewidth}
  \hfill \begin{tabular}{rc}
      \theaddress & \faMapMarker\\
    \theemail & \Letter\\
    \thephone & \faPhone\\
    \thebirth & \faBirthdayCake \\
    \thecountry& \faFlag
  \end{tabular}
%     \flushright
%     \theaddress \par
%     \theemail \par 
%     \thephone \par
  \end{minipage}\begin{minipage}{0.09\linewidth}
    \thepicture
  \end{minipage}
    \end{strip}
    \vskip 1.5em%
  \par
  \vskip 1.5em}
 
 \hypersetup{
    pdftitle={Curriculum vitae},    % título
    pdfsubject={Curriculum vitae},   % tema del documento
    colorlinks,
    breaklinks,
    urlcolor=links,
    linkcolor=links
} 
  
\makeatother
